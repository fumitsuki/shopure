<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Product" %>
<% List<Product> list = (List<Product>) request.getAttribute("productList"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>商品列表 - <%=request.getAttribute("projectName")%></title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-10 offset-md-1">
	<br>
	<% if(request.getParameter("category") != null){ %>
		<h1>搜尋類別：<%= request.getParameter("category") %></h1>
	<% }else if(request.getParameter("keyword") != null){ %>	
		<h1>搜尋標題關鍵字：<%= request.getParameter("keyword") %></h1>
	<%} else{ %>	
		<h1>所有商品：</h1>
	<% } %>
	<c:import url="msg.jsp">
	</c:import>	
		<% if(list.isEmpty()){ %>
				<p>不好意思～目前商品皆銷售一空！</p>
		<% }else{ %>	
			<div class="row">
			<% for(Product product:list){ %>
				<% if(product.getStatus() == "已上架"){ %>
				<div class="col-md-3"><div class="card product">
					<%if("".equals(product.getFilename())){ %>
				  		<img class="card-img-top" src="images/no_image.jpg" alt="<%= product.getName() %>" width="180">
				  	<% } else { %>
				  		<img class="card-img-top" src="images/<%= product.getFilename() %>" alt="<%= product.getName() %>" width="180">
				  	<% } %>
				  	<div class="card-body">
				  		<h4 class="card-text"><%= product.getName() %></h4>
				  		<% if(product.getSalePrice() != null && "".equals(product.getSalePrice()) != true){ %>
							<p class="card-text sale">優惠價 NT$ <%= product.getSalePrice()%> <span class="offerinsale">原價 <%= product.getOfferPrice()%></span></p>
						<% } else { %>
							<p class="card-text">NT$ <%= product.getOfferPrice()%></p>
						<% } %>
						<p class="card-text">分類： <a href="?category=<%= product.getCategory() %>"><%= product.getCategory()%></a></p>
						<a href="product?productId=<%= product.getId() %>" class="btn btn-success">商品詳情</a>   
						<a href="addCart?productId=<%= product.getId() %>" class="btn btn-primary">加入購物車</a> 	
				  	</div>
			  	</div></div>
				<% } %>
			<% } %>
			</div>
		<% } %>
</div></div>		
</body>
</html>