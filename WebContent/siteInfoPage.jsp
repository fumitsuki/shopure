<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>賣場資訊 - <%=request.getAttribute("projectName")%></title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
	<br>
	
	<h1>賣場資訊</h1>
	<c:import url="msg.jsp">
	</c:import>	
	<h3>關於本賣場</h3>
	<p><%= request.getAttribute("about") %></p>
	<br>
	<h3>購物流程</h3>
	<p><%= request.getAttribute("process") %></p>
	<br>
	<h3>匯款帳戶資訊</h3>
	<p>銀行名稱：<%= request.getAttribute("bankName") %></p>
	<p>帳號：<%= request.getAttribute("bankAccount") %></p>

</div></div>		
</body>
</html>