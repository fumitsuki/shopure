<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.Product" %>
<%@ page import="model.Cart" %>
<%@ page import="model.ProductUtil" %>
<%@ page import="java.util.HashMap" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>查看購物車 - <%=request.getAttribute("projectName")%></title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
	<br>
	<h1>我的購物車</h1>
	<c:import url="msg.jsp">
	</c:import>	
	<% Cart cart = (Cart) request.getAttribute("cart"); %>
	<% if(cart.getProductId().length != 0){ %>		
			<div class="card">
				<h5 class="card-header">購物車詳情</h5>
				<div class="list-group list-group-flush">
				<% HashMap<String, Product> productMap = (HashMap<String, Product>) request.getAttribute("productMap"); %>
				<% Integer pricetot = 0; %>
				<%for(int i = 0;i< cart.getProductId().length; i++){ %>
					<%Product p = productMap.get(cart.getProductId()[i]);%>
			  		<%Integer p2 = cart.getProductQty()[i];%>
				    <div class="list-group-item">
				    	<div class="row">
					    	<div class="col-md-3">
					    		<% if(p.getFilename() != null && "".equals(p.getFilename()) != true){ %>
					    		<img src="images/<%= p.getFilename() %>" alt="<%= p.getName() %>" width="100%"/>
					    		<% } else { %>
					    		<img src="images/no_image.jpg" alt="<%= p.getName() %>" width="100%"/>
					    		<% } %>
					    	</div>
					    	<div class="col-md-8">
					    		<p>產品名稱： <%= p.getName() %></p>
					    		<form class="form-inline" method="post" action="updateCart">
						    		<input type="hidden" class="form-control" name="productId" id="productId" value="<%= p.getId()  %>" >
						    		<div class="form-group mx-sm-3 mb-2">
					    				<label for="productQty">數量：</label>
		    							<input type="number" class="form-control" name="productQty" id="productQty" value="<%= p2  %>" >
		    						</div>
		    						<input type="submit" class="btn btn-primary mb-2" name="submit" value="更新">
					    		</form>
					    		<% Integer price = p.getSalePrice() != null && "".equals(p.getSalePrice()) != true ? Integer.parseInt(p.getSalePrice()) : Integer.parseInt(p.getOfferPrice());   %>
					    		<p>單價： <%= price %>&nbsp;&nbsp; <span class="font-weight-bold">總計： <%= price*p2 %></span> </p>
					    		<% pricetot += price*p2; %>
					    		<br>
					    		<a href="product?productId=<%= p.getId() %>" class="btn btn-success">商品詳情</a>   
					    		<a class="btn btn-danger" href="deleteCart?productId=<%= p.getId() %>">刪除</a>
					    	</div>
				    	</div>
				    </div>
				 <% } %>   
				 </div>
			</div>
			<br>
			<h4>所有商品合計：<%= pricetot %>&nbsp;&nbsp;&nbsp;<a href="orderCheck" class="btn btn-primary">結帳購買</a></h4>
			
	<% } else { %>
		<p>購物車目前是空的哦！</p>	
	<% } %>
	
</div></div>		
</body>
</html>