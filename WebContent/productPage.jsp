<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Product" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<% Product product = (Product) request.getAttribute("product"); %>
<title><%= product.getName() %> - <%=request.getAttribute("projectName")%></title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
	<br>
	<h1><%= product.getName() %></h1>
	<c:import url="msg.jsp">
	</c:import>	
	<div class="card">
		<% if(product.getFilename() != null && "".equals(product.getFilename()) != true){ %>
	 		<img class="card-img-top" src="images/<%= product.getFilename() %>" alt="<%= product.getName() %>">	
	 	<%} else { %>
	 		<h5 class="card-header">商品詳情</h5>
	 	<% } %>		  	
	  <div class="card-body">
	    <h5 class="card-title">產品名稱：<%= product.getName() %></h5>
	    <p class="card-text">商品描述：<%= product.getDescription() %></p>
	    
	    <% if(product.getSalePrice() != null && "".equals(product.getSalePrice()) != true){ %>
			<p class="card-text sale">優惠價 NT$ <%= product.getSalePrice()%> <span class="offerinsale">原價 <%= product.getOfferPrice()%></span></p>
		<% } else { %>
			<p class="card-text">NT$ <%= product.getOfferPrice()%></p>
		<% } %>
		
		<p class="card-text">分類： <%= product.getCategory()%> </p>  
		<% if(product.getStatus() == "已上架"){ %>
	    	<a href="addCart?productId=<%= product.getId() %>" class="btn btn-primary">加入購物車</a>
	    <% } %>	
	  </div>
	</div>		

</div></div>		
</body>
</html>