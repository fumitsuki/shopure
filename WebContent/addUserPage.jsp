<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% String p = request.getAttribute("page") != null ? (String) request.getAttribute("page") : "1" ;%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>帳戶註冊</title>
</head>
<body>
<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid">
<div class="col-md-8 offset-md-2">
<br>
	<h1>帳戶註冊(Page <%= p %>) - <%=request.getAttribute("projectName")%></h1>
	<c:import url="msg.jsp">
	</c:import>	
		<form name="form1" method="post" action="AddNewUser">
			<% if(p.equals("2")){ %>
			<p>就快完成了，填完就可以使用本網站的服務哦！</p>
			<div class="form-group">
    			<label for="name">姓名</label>
    			<input type="text" class="form-control" name="name" id="name" >
  			</div>
  			<div class="form-group">
    			<label for="address">住址</label>
    			<input type="text" class="form-control" name="address" id="address" >
  			</div>
			<div class="form-group">
    			<label for="email">Email</label>
    			<input type="email" class="form-control" name="email" id="email" >
  			</div>
			<div class="form-group">
    			<label for="phoneNumber">電話</label>
    			<input type="text" class="form-control" name="phoneNumber" id="phoneNumber" >
  			</div>
			
			<% } else {%>
			<p>給自己取個可愛又記得住的帳戶名稱吧～</p>
			<div class="form-group">
    			<label for="accountName">帳戶名稱</label>
    			<input type="text" class="form-control" name="accountName" id="accountName" >
  			</div>
			 <div class="form-group">
    			<label for="password">帳戶密碼</label>
    			<input type="text" class="form-control" name="password" id="password" >
  			</div>
			<% }%>				
			
			<input class="btn btn-secondary" type="reset" name="reset" value="重設">
			<input class="btn btn-primary" type="submit" name="submit" value="<%= p.equals("1") ? "下一頁" : "完成" %>">
		</form>
</div></div>	
</body>
</html>