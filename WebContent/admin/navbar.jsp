<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <% String projectName = request.getAttribute("projectName") != null ? ((String)request.getAttribute("projectName")) : "購物網站"; %>
  <a class="navbar-brand" href="#">${projectName}後台</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="products">商品管理</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="orders">訂單管理</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="../">回前台頁面</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="logout">管理者登出</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0"  name="/products" >
      <input class="form-control mr-sm-2" name="keyword" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">檢索商品</button>
    </form>
  </div>
</nav>