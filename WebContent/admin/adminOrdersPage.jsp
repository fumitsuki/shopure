<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Product" %>
<%@ page import="model.User" %>
<%@ page import="model.Order" %>
<%@ page import="model.ProductUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<% List<Order> list = (List<Order>) request.getAttribute("orderList"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>購物網站後台 訂單查詢</title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-12">
	<br>
	<h2>我的訂單</h2>
	<c:import url="msg.jsp">
		<c:param name="msg" value="${msg}" />
	</c:import>		
		<% if(list.isEmpty()){ %>
				<p>目前沒有任何訂單。</p>
		<% }else{ %>
			<table class="table">
			  	<thead>
			    	<tr>
			      		<th scope="col">編號</th>
			      		<th scope="col">訂購時間</th>
			      		<th scope="col">訂購人</th>
			      		<th scope="col">訂購產品</th>
			      		<th scope="col">總計</th>
			      		<th scope="col">訂單狀態</th>
			      		<th scope="col">操作</th>
			    	</tr>
			  	</thead>
			  	<tbody>
			<% HashMap<String, User> userMap = (HashMap<String, User>) request.getAttribute("userMap"); %>
			<% HashMap<String, Product> productMap = (HashMap<String, Product>) request.getAttribute("productMap"); %>  	
			<%for(Order order:list){ %>
				<% Integer pricetot = 0; %>
			  	<tr>
			      	<th scope="row"><small><%= order.getPort().substring(0,8) %></small></th>
			      	<% Date date = order.getDate(); %>
			      	<td><small><%= new SimpleDateFormat("MM-dd-yyyy").format(date)  %></small></td>
			      	<% User user = userMap.get(order.getAccountName()); %>
			      	<td>
			      		<p>姓名：<%= user.getName() %>&nbsp;&nbsp;電話：<%= user.getPhoneNumber() %></p>
			      		<p>地址：<%= user.getAddress() %></p>
			      	</td>
			      	<td>
			      	<% for(int i=0;i< order.getProductId().length;i++){%>
			      		<%Product p = productMap.get(order.getProductId()[i]);%>
				  		<%Integer p2 = order.getProductQty()[i];%>
				  		<% Integer price = p.getSalePrice() != null && "".equals(p.getSalePrice()) != true ? Integer.parseInt(p.getSalePrice()) : Integer.parseInt(p.getOfferPrice());   %>
			      		<p><%= p.getName() %> (單價 <%= price %>) x <%= p2 %> = <span class="font-weight-bold">總計： <%= price*p2 %></span></p>
					    <% pricetot += price*p2; %>
					<% } %>    
					</td>
			      	<td><%= pricetot %></td>
			      	<td><%= order.getStatus() %></td>
			      	<td>
			      	<% if(order.getStatus().equals("賣家未收到款項")){ %>
			      		<a href="UpdateOrderStatus?port=<%= order.getPort() %>" class="btn btn-success">確認收款</a>
			      	<% }else if(order.getStatus().equals("尚未寄出貨品")){ %>
			      		<a href="UpdateOrderStatus?port=<%= order.getPort() %>" class="btn btn-primary">確認寄貨</a>	
			      	<% } %>	
			      	</td>
			    </tr>
			    <% } %>
			<tbody>
		</table>	
		<% } %>
</div></div>		
</body>
</html>