<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Product" %>
<% List<Product> list = (List<Product>) request.getAttribute("productList"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>購物網站後台 商品列表</title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<br>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
	<% if(request.getParameter("category") != null){ %>
		<h2>搜尋類別：<%= request.getParameter("category") %></h2>
	<% }else if(request.getParameter("keyword") != null){ %>	
		<h2>搜尋標題關鍵字：<%= request.getParameter("keyword") %></h2>
	<%} else{ %>	
		<h2>所有商品：</h2>
	<% } %>
	<c:import url="msg.jsp">
		<c:param name="msg" value="${msg}" />
	</c:import>	
		<% if(list.isEmpty()){ %>
				<p>目前沒有商品</p>
		<% }else{ %>
			<table class="table">
			  	<thead>
			    	<tr>
			      		<th scope="col">#</th>
			      		<th scope="col">圖片</th>
			      		<th scope="col">產品名稱</th>
			      		<th scope="col">成本價格</th>
			      		<th scope="col">售價</th>
			      		<th scope="col">優惠價</th>
			      		<th scope="col">類別</th>
			      		<th scope="col">動作</th>
			    	</tr>
			  </thead>
			  <tbody>
			  <% int num = 0; %>
			  <% for(Product product:list){ %>
			  <% num += 1; %>
			    	<tr>
			      		<th scope="row"><%= num %></th>
			      		<td>
							<% if(product.getFilename() != null && "".equals(product.getFilename()) != true){ %>
								<img src="../images/<%= product.getFilename() %>" alt="<%= product.getName() %>" height="50"/>
							<% } %>
						</td>
			      		<td><%= product.getName() %></td>
			      		<td><%= product.getCostPrice() %></td>
			      		<td><%= product.getOfferPrice() %></td>
			      		<td><%= product.getSalePrice() %></td>
			      		<td><a href="?category=<%= product.getCategory() %>"><%= product.getCategory() %></a></td>
			      		<td>
			      			<a href="updateProduct?productId=<%= product.getId() %>" class="btn btn-primary">修改</a>
			      			<% String status = product.getStatus() == "已上架" ? "下架" : "上架"; %>
			      			<a href="updateProductStatus?productId=<%= product.getId() %>" class="btn btn-danger"><%= status %></a>
			      		</td>
			    	</tr>
			   <% } %>
			   </tbody>
			 </table>   		
		<% } %>
		<br>
		<br>	
		<div class="col-md-8 offset-md-2"><div class="card">
		<div class="card-header">新增訂單</div>
		<div class="card-body"><form method="post" action="addProduct" enctype="multipart/form-data">
		 	<div class="form-group">
		    	<label for="name">產品名稱</label>
		    	<input type="text" class="form-control" name="name" id="name" >
		    	<small id="nameHelp" class="form-text text-muted">注意產品名不可為空</small>
		  	</div>
		  	<div class="form-group">
    			<label for="description">產品描述</label>
    			<textarea class="form-control" id="description" name="description" rows="3"></textarea>
  			</div>
		  	<div class="form-group">
		    	<label for="costPrice">成本價</label>
		    	<input type="number" class="form-control" name="costPrice" id="costPrice" >
		  	</div>
		  	<div class="form-group">
		    	<label for="offerPrice">售價</label>
		    	<input type="number" class="form-control" name="offerPrice" id="offerPrice" >
		  	</div>
		  	<div class="form-group">
		    	<label for="salePrice">優惠價</label>
		    	<input type="number" class="form-control" name="salePrice" id="salePrice" >
		  	</div>
		  	<div class="form-group">
		    	<label for="category">分類</label>
		    	<input type="text" class="form-control" name="category" id="category" >
		  	</div>
			<div class="form-group">
			    <label for="file">圖片上傳</label>
			    <input type="file" class="form-control-file" name="file" id="file">
			</div>
		  	<input type="submit" class="btn btn-primary" name="submit" value="送出">
		</form></div>
		</div></div>	
</div></div>		
</body>
</html>