<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Product" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>購物網站後台 更新商品資訊</title>
</head>
<body>
<c:import url="navbar.jsp">
</c:import>
<br>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
	<c:import url="msg.jsp">
		<c:param name="msg" value="${msg}" />
	</c:import>	
		<br>
		<br>	
		<div class="col-md-8 offset-md-2"><div class="card">
		<div class="card-header">修改訂單</div>
		<% Product product = (Product) request.getAttribute("product"); %>
		<div class="card-body"><form method="post" action="updateProduct" enctype="multipart/form-data">
			<input type="hidden" class="form-control" name="productId" id="productId"  value="<%= product.getId() %>" >
		 	<div class="form-group">
		    	<label for="name">產品名稱</label>
		    	<input type="text" class="form-control" name="name" id="name" value="<%= product.getName() %> " >
		    	<small id="nameHelp" class="form-text text-muted">注意產品名不可為空</small>
		  	</div>
		  	<div class="form-group">
    			<label for="description">產品描述</label>
    			<textarea class="form-control" id="description" name="description" rows="3"><%= product.getDescription() %></textarea>
  			</div>
		  	<div class="form-group">
		    	<label for="costPrice">成本價</label>
		    	<input type="number" class="form-control" name="costPrice" id="costPrice" value="<%= product.getCostPrice() %>" >
		  	</div>
		  	<div class="form-group">
		    	<label for="offerPrice">售價</label>
		    	<input type="number" class="form-control" name="offerPrice" id="offerPrice" value="<%= product.getOfferPrice() %>" >
		  	</div>
		  	<div class="form-group">
		    	<label for="salePrice">優惠價</label>
		    	<input type="number" class="form-control" name="salePrice" id="salePrice" value="<%= product.getSalePrice() %>" >
		  	</div>
		  	<div class="form-group">
		    	<label for="category">分類</label>
		    	<input type="text" class="form-control" name="category" id="category" value="<%= product.getCategory() %>">
		  	</div>
			<div class="form-group">
			    <label for="file">圖片上傳</label>
			    <% if(product.getFilename() != null && "".equals(product.getFilename()) != true){ %>
			    <img src="../images/<%= product.getFilename() %>" alt="<%= product.getName() %>" height="100"/>
			    <% } %>
			    <input type="file" class="form-control-file" name="file" id="file"  >
			</div>
		  	<input type="submit" class="btn btn-primary" name="submit" value="更新">
		</form></div>
		</div></div>	
</div></div>		
</body>
</html>