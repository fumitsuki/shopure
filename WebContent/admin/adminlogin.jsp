<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>後台登入頁面</title>
</head>
<body>
<div class="container-fluid"><div class="col-md-6 offset-md-3">
	<br>
	<h1>後台登入頁面</h1>
	<p>如果您誤闖這個頁面，請勿嘗試登入，避免觸法，謝謝！</p>	
	<c:import url="msg.jsp">
		<c:param name="msg" value="${msg}" />
	</c:import>	
	<form action="login" method="post" name="login">
		<div class="form-group">
	    	<label for="adminAccountName">帳戶名稱</label>
	    	<input type="text" class="form-control" name="adminAccountName" id="adminAccountName" >
	  	</div>
		<div class="form-group">
	    	<label for="adminPassword">帳戶密碼</label>
	    	<input type="password" class="form-control" name="adminPassword" id="adminPassword" >
	  	</div>
		<input class="btn btn-primary" type="submit" name="submit" value="送出">
		<a class="btn btn-success" href="../">回一般頁面</a><br>
	</form>
</div></div>	
</body>
</html>