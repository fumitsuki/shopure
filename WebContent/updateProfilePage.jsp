<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.User" %>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>帳戶資料修改 - <%=request.getAttribute("projectName")%></title>
</head>
<body>
<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid">
<div class="col-md-8 offset-md-2">
<br>
	<h1>帳戶資料修改</h1>
	<c:import url="msg.jsp">
	</c:import>	
		<form name="form1" method="post" action="UpdateProfile">
			<p>就快完成了，填完就可以使用本網站的服務哦！</p>
			<% User user = (User) request.getAttribute("user"); %>
			<div class="form-group">
    			<label for="name">姓名</label>
    			<input type="text" class="form-control" name="name" id="name" value="<%= user.getName() %>" >
  			</div>
  			<div class="form-group">
    			<label for="address">住址</label>
    			<input type="text" class="form-control" name="address" id="address" value="<%= user.getAddress() %>">
  			</div>
			<div class="form-group">
    			<label for="email">Email</label>
    			<input type="email" class="form-control" name="email" id="email" value="<%= user.getEmail() %>">
  			</div>
			<div class="form-group">
    			<label for="phoneNumber">電話</label>
    			<input type="text" class="form-control" name="phoneNumber" id="phoneNumber" value="<%= user.getPhoneNumber() %>" >
  			</div>		
			<input class="btn btn-primary" type="submit" name="submit" value="更改">
			<a class="btn btn-danger" href="logout">登出</a>
		</form>
</div></div>	
</body>
</html>