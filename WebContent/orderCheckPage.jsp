<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.Product" %>
<%@ page import="model.Cart" %>
<%@ page import="model.User" %>
<%@ page import="model.ProductUtil" %>
<%@ page import="java.util.HashMap" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>訂單確認 - <%=request.getAttribute("projectName")%></title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
	<h2>我的訂單</h2>
	<c:import url="msg.jsp">
	</c:import>	
	<% Cart cart = (Cart) request.getAttribute("cart"); %>	
		<div class="card">
			<h5 class="card-header">訂購商品詳情</h5>
			<div class="list-group list-group-flush">
			<% HashMap<String, Product> productMap = (HashMap<String, Product>) request.getAttribute("productMap"); %>
			<% Integer pricetot = 0; %>
			<%for(int i = 0;i< cart.getProductId().length; i++){ %>
				<%Product p = productMap.get(cart.getProductId()[i]);%>
			  	<%Integer p2 = cart.getProductQty()[i];%>
				<div class="list-group-item">
				    <div class="row">
					   <div class="col-md-3">
					    	<% if(p.getFilename() != null && "".equals(p.getFilename()) != true){ %>
					    	<img src="images/<%= p.getFilename() %>" alt="<%= p.getName() %>" width="100%"/>
					   		<% } else { %>
					   		<img src="images/no_image.jpg" alt="<%= p.getName() %>" width="100%"/>
					   		<% } %>
					    </div>
					    <div class="col-md-8">
					    	<p>產品名稱： <%= p.getName() %></p>
					    	<p>數量： <%= p2 %></p>
					    	<% Integer price = p.getSalePrice() != null && "".equals(p.getSalePrice()) != true ? Integer.parseInt(p.getSalePrice()) : Integer.parseInt(p.getOfferPrice());   %>
					    	<p>單價： <%= price %>&nbsp;&nbsp; <span class="font-weight-bold">總計： <%= price*p2 %></span> </p>
					    	<% pricetot += price*p2; %>
					    </div>
				    </div>
				 </div>
				 <% } %>
				 <div class="list-group-item">
					 	<h4>所有商品合計：<%= pricetot %></h4>
				</div>   
			</div>
		</div>
		<br>
		<div class="card">
			<h5 class="card-header">訂單資訊</h5>
			<div class="card-body">
				<% User user = (User) request.getAttribute("user"); %>
				<p class="card-title">請確認以下資訊是否無誤：</p>
				<p class="card-text">收件人姓名：<%= user.getName() %></p>
				<p class="card-text">收件人電話：<%= user.getPhoneNumber() %></p>
				<p class="card-text">收件人地址：<%= user.getAddress() %></p>
				<p class="card-text">收件人Email：<%= user.getEmail() %></p>
			</div>
		</div>
		<br>
		<a href="addOrder" class="btn btn-primary">確認下訂</a>
</div></div>		
</body>
</html>