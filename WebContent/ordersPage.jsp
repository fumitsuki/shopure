<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Product" %>
<%@ page import="model.Order" %>
<%@ page import="model.ProductUtil" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<% List<Order> list = (List<Order>) request.getAttribute("orderList"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>訂單查詢 - <%=request.getAttribute("projectName")%></title>
</head>
<body>

<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
	<br>
	<h1>我的訂單</h1>
	<c:import url="msg.jsp">
	</c:import>		
		<% if(list.isEmpty()){ %>
				<p>目前沒有任何訂單喔！</p>
		<% }else{ %>
			<%for(Order order:list){ %>		
			<div class="card">
				<h5 class="card-header">訂單 #<%= order.getPort().substring(0,8) %></h5>
				<div class="list-group list-group-flush">
					<div class="list-group-item">
						<% Date date = order.getDate(); %>
						<p class="card-text">下訂日期：<%= new SimpleDateFormat("MM-dd-yyyy").format(date) %></p>
						<p class="card-text">訂單狀態：<%= order.getStatus() %></p>
					</div>	
					<div class="list-group-item">
					<% HashMap<String, Product> productMap = (HashMap<String, Product>) request.getAttribute("productMap"); %>
					<% Integer pricetot = 0; %>
					<% for(int i=0;i< order.getProductId().length;i++){%>
				  		<%Product p = productMap.get(order.getProductId()[i]);%>
				  		<%Integer p2 = order.getProductQty()[i];%>
				  		<div class="row">
							<div class="col-md-3">
					    		<% if(p.getFilename() != null && "".equals(p.getFilename()) != true){ %>
					    		<img src="images/<%= p.getFilename() %>" alt="<%= p.getName() %>" width="100%"/>
					    		<% } else { %>
					    		<img src="images/no_image.jpg" alt="<%= p.getName() %>" width="100%"/>
					    		<% } %>
					    	</div>
						    <div class="col-md-8">
						    	<% if(p.getStatus() == "已上架"){ %>
						    	<p>產品名稱： <a href="product?productId=<%= p.getId() %>"><%= p.getName() %></a></p>
						    	<% } else { %>
						    	<p>產品名稱： <%= p.getName() %></p>
						    	<% } %>
						    	<p>數量： <%= p2 %></p>
						    	<% Integer price = p.getSalePrice() != null && "".equals(p.getSalePrice()) != true ? Integer.parseInt(p.getSalePrice()) : Integer.parseInt(p.getOfferPrice());   %>
						    	<p>單價： <%= price %>&nbsp;&nbsp; <span class="font-weight-bold">總計： <%= price*p2 %></span> </p>
					    		<% pricetot += price*p2; %>
						    </div>
						 </div>
						 <br>
					<% } %>
				  	</div>
				  	<div class="list-group-item">
				  		<h5>所有商品合計： NT$ <%= pricetot %> </h5>
				  	</div>
			  	</div>
			</div>
			<br>
			<% } %>
		<% } %>
</div></div>		
</body>
</html>