<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="user" class="model.User" scope="request" /> 
<!-- Bootstrap  -->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>帳戶註冊成功 - <%=request.getAttribute("projectName")%></title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
</head>
<body>
<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-8 offset-md-2">
<br>
	<h1>帳戶註冊成功</h1>
	<p>太好了，帳戶註冊成功！可以趕快登入購物了！</p>
	<div class="card">
		<h5 class="card-header">帳戶資訊</h5>
		<div class="card-body">
	   		<p class="card-text">帳號名稱：${user.accountName}</p>
	   		<p class="card-text">姓名：${user.name}</p>	
	   		<p class="card-text">住址：${user.address}</p>	
	   		<p class="card-text">Email：${user.email}</p>
	   		<p class="card-text">電話：${user.phoneNumber}</p>    		
		</div>
	</div>	
	<br>
	<a class="btn btn-primary" href='UserLogin'>回登入頁面</a>

</div></div>

</body>
</html>