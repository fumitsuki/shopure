<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<link href="<%=request.getContextPath()%>/css/stylesheet.css" rel="stylesheet">
<title>會員登入 - <%=request.getAttribute("projectName")%></title>

</head>
<body>
<c:import url="navbar.jsp">
</c:import>
<div class="container-fluid"><div class="col-md-6 offset-md-3">
	<br>
	<h1>登入頁面</h1>
	<c:import url="msg.jsp">
	</c:import>	
	<p>歡迎使用購物網站，請用您的個人資料來登入：</p>
	<form action="UserLogin" method="post" name="login">
		<div class="form-group">
	    	<label for="accountName">帳戶名稱</label>
	    	<input type="text" class="form-control" name="accountName" id="accountName" >
	  	</div>
		<div class="form-group">
	    	<label for="password">帳戶密碼</label>
	    	<input type="password" class="form-control" name="password" id="password" >
	  	</div>
		<input class="btn btn-primary" type="submit" name="submit" value="送出">
		<a class="btn btn-success" href="AddNewUser">註冊</a><br>
	</form>
	
</div></div>	
</body>
</html>