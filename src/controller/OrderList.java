package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.ProductUtil;
import model.CookieUtil;
import model.OrderUtil;
import model.User;
import model.Product;
import model.Order;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/orders")
public class OrderList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au =  new AccountUtil();
		ProductUtil pu =  new ProductUtil();
		CookieUtil cu =  new CookieUtil();
		OrderUtil ou = new OrderUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		HashMap<String, Product> productMap = pu.mapInit((HashMap<String, Product>) getServletContext().getAttribute("productMap"));
		HashMap<String, Order> orderMap = ou.mapInit((HashMap<String, Order>) getServletContext().getAttribute("orderMap"));
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = au.LoginCheck(cookieMap, userMap);
		request.setAttribute("login", login);
		String jspPageToForward;
		
		if(login) {	
			jspPageToForward = "ordersPage.jsp";
			User user = au.getLoginUser(cookieMap, userMap);
			List<Order> orderList = ou.getOrderListByUser(user.getAccountName(), orderMap);
			request.setAttribute("orderList", orderList);
			request.setAttribute("productMap",productMap);
		}
		else {
			jspPageToForward = "UserLogin";
			request.setAttribute("msg", "請先登入後再查看訂單！");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
