package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import model.AdminAccountUtil;
import model.CookieUtil;
import model.ProductUtil;
import model.Product;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@MultipartConfig(location="/Users/tsaijuming/eclipse-workspace/final_project/WebContent/images")
@WebServlet("/admin/updateProduct")
public class AdminUpdateProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AdminAccountUtil aau = new AdminAccountUtil();
		CookieUtil cu = new CookieUtil();
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = aau.LoginCheck(cookieMap);
		
		ProductUtil pu =  new ProductUtil();
		HashMap<String, Product> productMap = pu.mapInit((HashMap<String, Product>) getServletContext().getAttribute("productMap"));
		String submit = request.getParameter("submit") == null ? "" : request.getParameter("submit");
		String jspPageToForward;
		String id;
		
		if (login == true) { /*已自動登入*/
			if ("更新".equals(submit)) {
				System.out.println("submit:"+ submit);  /*debugging*/
				String filename = "";
				if(request.getPart("file") != null) {
					Part part = request.getPart("file");
					filename = getFilename(part);
					if(filename != null && "".equals(filename) != true) {
						part.write(filename);
					}
				}
				id = request.getParameter("productId");
				String name = request.getParameter("name");
				if(name == null) {
					request.setAttribute("msg", "產品名稱不可為空！");
				}
				else if(name.equals("")) {
					request.setAttribute("msg", "產品名稱不可為空！");
				}
				else {
					String description = request.getParameter("description");
					String costPrice = request.getParameter("costPrice");
					String salePrice = request.getParameter("salePrice");
					String offerPrice = request.getParameter("offerPrice");
					String category = request.getParameter("category");
					filename = "".equals(filename) ? pu.getProduct(id, productMap).getFilename() : filename;
					boolean productup = pu.updateProduct(id,name, description, filename, costPrice, salePrice, offerPrice, category, productMap);
					getServletContext().setAttribute("productMap",productMap);
					
					String msg = productup ?  "產品更新成功！" : "產品更新失敗！請注意產品代碼是否正確。";
					request.setAttribute("msg", msg);
				}
				jspPageToForward = "";
			}
			else {
				System.out.println("submit != 更新，進入else");  /*debugging*/
				id = request.getParameter("productId");
				
				if(id == null) {
					request.setAttribute("msg", "需要產品代號才能修改！");
					jspPageToForward = "";
				}
				else if(id.equals("")) {
					request.setAttribute("msg", "需要產品代號才能修改！");
					jspPageToForward = "";
				}
				else {
					System.out.println("submit != 更新，進入else");  /*debugging*/
					Product product = pu.getProduct(id, productMap);
					request.setAttribute("product", product);
					jspPageToForward = "adminAlterProduct.jsp";
				}
			}
		} else {
			request.setAttribute("msg", "請先至後台頁面登入！");
			jspPageToForward = "login";
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}
	
	private String getFilename(Part part) {
		String header = part.getHeader("Content-Disposition");
		String filename = header.substring(
				header.indexOf("filename=\"") + 10,
				header.lastIndexOf("\"")
		);
		return filename;
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
