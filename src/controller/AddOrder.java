package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.CookieUtil;
import model.Order;
import model.OrderUtil;
import model.Cart;
import model.CartUtil;
import model.ProductUtil;
import model.User;
import model.Product;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/addOrder")
public class AddOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au =  new AccountUtil();
		ProductUtil pu =  new ProductUtil();
		CookieUtil cu =  new CookieUtil();
		OrderUtil ou = new OrderUtil();
		CartUtil cau = new CartUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		HashMap<String, Cart> cartMap = cau.mapInit((HashMap<String, Cart>) getServletContext().getAttribute("cartMap"));
		HashMap<String, Order> orderMap = ou.mapInit((HashMap<String, Order>) getServletContext().getAttribute("orderMap"));
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = au.LoginCheck(cookieMap, userMap);
		request.setAttribute("login", login);
		String jspPageToForward;
		
		if(login) {	
			jspPageToForward = "orders";
			User user = au.getLoginUser(cookieMap, userMap);
			Cart cart = cau.cartInit(user.getAccountName(),cartMap);
			ou.addNewOrder(cart.getProductId(), cart.getProductQty(), user.getAccountName(), orderMap);
			cau.cleanCart(user.getAccountName(), cartMap);	
			getServletContext().setAttribute("cartMap", cartMap);
			getServletContext().setAttribute("orderMap", orderMap);
			response.sendRedirect(jspPageToForward);
		}
		else {
			jspPageToForward = "UserLogin";
			request.setAttribute("msg", "請先登入後再加入購物車！");
			RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
			dispatcher.forward(request, response);
		}
		
	}
	

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
