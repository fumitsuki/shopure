package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.CookieUtil;
import model.User;


/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/UserLogin")
public class UserLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String jspPageToForward = null;
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		AccountUtil au = new AccountUtil();
		CookieUtil cu = new CookieUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		
		// 取得submit參數, 看Client端是第一次呼叫, 還是填寫完資料後的呼叫
		String accountName = request.getParameter("accountName"); 
		String password = request.getParameter("password");
		Boolean submit = request.getParameter("submit") != null ? true : false;
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = au.LoginCheck(cookieMap, userMap);
		request.setAttribute("login", login);
		System.out.println(userMap);  /*debugging*/
		System.out.println("submit: "+submit);  /*debugging*/
		System.out.println("accountName: "+accountName);  /*debugging*/
		System.out.println("password: "+ password);  /*debugging*/
		
		if (login == true) { /*已自動登入*/
			System.out.println("已自動登入");  /*debugging*/
			accountName = cookieMap.get("accountName");
			jspPageToForward = "products";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		}
		else if (accountName == "" && password == "" && submit == true ) {
			request.setAttribute("msg", "請輸入帳號與密碼");
			jspPageToForward = "login.jsp";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		}
		else if (accountName == null && password == null ) {
			jspPageToForward = "login.jsp";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		}
		else if (au.checkAccountNameExistence(accountName, userMap) == false) {
			request.setAttribute("msg", "輸入帳號錯誤，查無此帳號");
			jspPageToForward = "login.jsp";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		}
		else if (au.checkPassword(accountName, password, userMap) == false) {
			request.setAttribute("msg", "輸入密碼錯誤");
			jspPageToForward = "login.jsp";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		}
		else { /*成功登入*/
			User user = au.getUser(accountName, userMap);
			request.setAttribute("login", true);
			Cookie loginCookie = new Cookie("accountName",accountName);
			loginCookie.setMaxAge(24 * 60 * 60); /* 單位是秒 */
			response.addCookie(loginCookie);
			loginCookie = new Cookie("password",password);
			loginCookie.setMaxAge(24 * 60 * 60); /* 單位是秒 */
			response.addCookie(loginCookie);
			
			request.setAttribute("user", user);
			jspPageToForward = "products";
			response.sendRedirect(jspPageToForward);
		}
		
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	
	
}
