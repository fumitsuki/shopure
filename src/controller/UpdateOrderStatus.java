package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.AdminAccountUtil;
import model.ProductUtil;
import model.CookieUtil;
import model.OrderUtil;
import model.User;
import model.Product;
import model.Order;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/admin/UpdateOrderStatus")
public class UpdateOrderStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au = new AccountUtil();
		AdminAccountUtil aau = new AdminAccountUtil();
		ProductUtil pu =  new ProductUtil();
		CookieUtil cu =  new CookieUtil();
		OrderUtil ou = new OrderUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		HashMap<String, Order> orderMap = ou.mapInit((HashMap<String, Order>) getServletContext().getAttribute("orderMap"));
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = aau.LoginCheck(cookieMap);
		String port = request.getParameter("port");
		
		String jspPageToForward;
		
		if(login) {	
			if(port == null) {
				request.setAttribute("msg", "需要訂單編號才能修改！");
				jspPageToForward = "";
			}
			else if("".equals(port)) {
				request.setAttribute("msg", "需要訂單編號才能修改！");
				jspPageToForward = "";
			}
			else {
				System.out.println("進入修改訂單狀態的條件");  /*debugging*/
				ou.updateOrderStatus(port, orderMap);
				request.setAttribute("msg", "需要訂單狀態修改成功！");
			}
			
			jspPageToForward = "orders";
		}
		else {
			jspPageToForward = "login";
			request.setAttribute("msg", "請先登入後再修改訂單狀態！");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
