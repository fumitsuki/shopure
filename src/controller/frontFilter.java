package controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class frontFilter extends HttpFilter {
    @Override
    protected void doFilter(
         HttpServletRequest request, HttpServletResponse response, FilterChain chain)
                throws IOException, ServletException {
    	/*設定編碼*/
    	request.setCharacterEncoding("utf-8");
    	response.setCharacterEncoding("utf-8");
    	/*帶入店家名稱*/
    	String projectName = getInitParameter("projectName"); 
    	projectName = projectName != null ? projectName : "購物網站" ;
    	request.setAttribute("projectName", projectName);
        chain.doFilter(request, response);
    	
    }
}