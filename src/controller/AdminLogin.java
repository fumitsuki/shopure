package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AdminAccountUtil;
import model.CookieUtil;


/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/admin/login")
public class AdminLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String jspPageToForward = null;
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		CookieUtil cu = new CookieUtil();
		AdminAccountUtil aau = new AdminAccountUtil();
		
		String adminAccountName = request.getParameter("adminAccountName"); 
		String adminPassword = request.getParameter("adminPassword");
		Boolean submit = request.getParameter("submit") != null ? true : false;
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = aau.LoginCheck(cookieMap);
		
		System.out.println("submit: "+ submit);  /*debugging*/
		System.out.println("adminAccountName: "+ adminAccountName);  /*debugging*/
		System.out.println("adminPassword: "+ adminPassword);  /*debugging*/
		
		if (login == true) { /*已自動登入*/
			System.out.println("已自動登入");  /*debugging*/
			jspPageToForward = "/admin/products";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		} else if (adminAccountName == null && adminPassword == null ) {
			jspPageToForward = "adminlogin.jsp";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		}
		else if (aau.checkAccount(adminAccountName, adminPassword) == false) {
			request.setAttribute("msg", "輸入帳號密碼錯誤");
			jspPageToForward = "adminlogin.jsp";
			request.getRequestDispatcher(jspPageToForward).forward(request, response);
		}
		else { /*成功登入*/
			Cookie loginCookie = new Cookie("adminAccountName",adminAccountName);
			loginCookie.setMaxAge(24 * 60 * 60); /* 單位是秒 */
			response.addCookie(loginCookie);
			loginCookie = new Cookie("adminPassword",adminPassword);
			loginCookie.setMaxAge(24 * 60 * 60); /* 單位是秒 */
			response.addCookie(loginCookie);
			response.sendRedirect("products");
		}
		
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	
	
}
