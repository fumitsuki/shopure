package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.CookieUtil;
import model.Cart;
import model.CartUtil;
import model.ProductUtil;
import model.User;
import model.Product;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/addCart")
public class AddCart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au =  new AccountUtil();
		ProductUtil pu =  new ProductUtil();
		CookieUtil cu =  new CookieUtil();
		CartUtil cau = new CartUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		HashMap<String, Cart> cartMap = cau.mapInit((HashMap<String, Cart>) getServletContext().getAttribute("cartMap"));
		HashMap<String, Product> productMap = pu.mapInit((HashMap<String, Product>) getServletContext().getAttribute("productMap"));
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = au.LoginCheck(cookieMap, userMap);
		request.setAttribute("login", login);
		
		String jspPageToForward;
		String productId = request.getParameter("productId");
		String productQty = request.getParameter("productQty");
		Integer pQty = 0;
		if(productQty != null && "".equals(productQty) != true) {
			pQty = Integer.parseInt(productQty);		
		}
		
		if(login) {	
			System.out.println("productId:" + productId); /*debugging*/
			if(productId != null) {
				User user = au.getLoginUser(cookieMap, userMap);
				if(pQty > 0) {
					cau.addNewProduct(user.getAccountName(), productId, pQty, cartMap);
				}
				else if(pu.getProduct(productId,productMap).getStatus() == "未上架" ) {
					request.setAttribute("msg", "購物車新增失敗！產品狀態無法購買！");
					jspPageToForward = "myCart";
				}else {
					cau.addNewProduct(user.getAccountName(), productId, 1, cartMap);
				}
				getServletContext().setAttribute("cartMap",cartMap);
				request.setAttribute("msg", "購物車新增成功！");
				jspPageToForward = "myCart";
			}
			else {
				request.setAttribute("msg", "購物車新增失敗！產品代號有誤！");
				jspPageToForward = "myCart";
			}
		}
		else {
			jspPageToForward = "UserLogin";
			request.setAttribute("msg", "請先登入後再加入購物車！");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
		
	}
	

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
