package controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.User;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/AddNewUser")
public class AddNewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au =  new AccountUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		String jspPageToForward = null;
		String page = request.getParameter("submit") == null ? "" : request.getParameter("submit");
		request.setAttribute("page", page.equals("下一頁") ? "2" : "1");
		request.setAttribute("login", false);
		
		if ("完成".equals(page)) {	
			String name = request.getParameter("name");
			String address = request.getParameter("address");
			String email = request.getParameter("email");
			String phoneNumber = request.getParameter("phoneNumber");
			String accountName = (String) request.getSession().getAttribute("accountName");
			String password = (String) request.getSession().getAttribute("password");
				
			User user = au.addNewUser(accountName, password, name, address, email, phoneNumber, userMap);
			request.setAttribute("user", user);
			getServletContext().setAttribute("userMap",userMap);
			request.getSession().invalidate();
			jspPageToForward = "infoCheckPage.jsp";
		}
		else if("下一頁".equals(page)) {
			
			String accountName =  request.getParameter("accountName");
			String password =  request.getParameter("password");
			System.out.println(userMap);  /*debugging*/
			if("".equals(accountName)) {
				request.setAttribute("msg","帳戶名稱不可為空！");
				request.setAttribute("page", "1");
				jspPageToForward = "addUserPage.jsp";
			}
			else if(au.checkAccountNameExistence(accountName, userMap) == false) {
				request.getSession().setAttribute("accountName", accountName);
				request.getSession().setAttribute("password", password);
				jspPageToForward = "addUserPage.jsp";
			}
			else {
				request.setAttribute("msg","真是不巧，這個帳號已經有人使用了！再取一個不一樣的吧～");
				request.setAttribute("page", "1");
				jspPageToForward = "addUserPage.jsp";
			}
		}
		else {
			jspPageToForward = "addUserPage.jsp";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
