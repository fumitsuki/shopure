package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AdminAccountUtil;
import model.CookieUtil;
import model.ProductUtil;
import model.Product;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/admin/updateProductStatus")
public class AdminUpdateProductStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AdminAccountUtil aau = new AdminAccountUtil();
		CookieUtil cu = new CookieUtil();
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = aau.LoginCheck(cookieMap);
		
		ProductUtil pu =  new ProductUtil();
		HashMap<String, Product> productMap = pu.mapInit((HashMap<String, Product>) getServletContext().getAttribute("productMap"));
		String jspPageToForward;
		
		if (login == true) { /*已自動登入*/
			String id = request.getParameter("productId");
			if(id == null) {
				request.setAttribute("msg", "需要產品代號才能更新狀態！");
			}
			else if(id.equals("")) {
				request.setAttribute("msg", "需要產品代號才能更新狀態！");
			}
			else {
				boolean productdel = pu.updateProductStatus(id,productMap);
				getServletContext().setAttribute("productMap",productMap);
				String msg = productdel ?  "產品更新成功！" : "產品更新失敗！請注意產品代碼是否正確。";
				request.setAttribute("msg", msg);
			}
			jspPageToForward = "";
		} else {
			request.setAttribute("msg", "請先至後台頁面登入！");
			jspPageToForward = "login";
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
