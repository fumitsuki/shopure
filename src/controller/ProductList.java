package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.CookieUtil;
import model.ProductUtil;
import model.User;
import model.Product;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/products")
public class ProductList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au =  new AccountUtil();
		ProductUtil pu =  new ProductUtil();
		CookieUtil cu =  new CookieUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		HashMap<String, Product> productMap = pu.mapInit((HashMap<String, Product>) getServletContext().getAttribute("productMap"));
		List<Product> productList = new ArrayList<>();
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = au.LoginCheck(cookieMap, userMap);
		request.setAttribute("login", login);
		String jspPageToForward = "productsPage.jsp";
		
		if(request.getParameter("category") != null) {	
			String cat = request.getParameter("category") == null ? "" : request.getParameter("category");
			productList = pu.getProductListByCategory(cat, productMap);
		}
		else if(request.getParameter("keyword") != null) {
			String kw = request.getParameter("keyword") == null ? "" : request.getParameter("keyword");		
			productList = pu.getProductListByKeyword(kw, productMap);
		}
		else {
			productList = pu.getProductList(productMap);
		}
		
		request.setAttribute("productList",productList);
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
