package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AdminAccountUtil;
import model.CookieUtil;
import model.ProductUtil;
import model.Product;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/admin/products")
public class AdminProductList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AdminAccountUtil aau = new AdminAccountUtil();
		CookieUtil cu = new CookieUtil();
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = aau.LoginCheck(cookieMap);
		
		ProductUtil pu =  new ProductUtil();
		HashMap<String, Product> productMap = pu.mapInit((HashMap<String, Product>) getServletContext().getAttribute("productMap"));
		List<Product> productList = new ArrayList<>();
		String jspPageToForward;
		
		if (login == true) { /*已自動登入*/
			jspPageToForward = "adminProductsPage.jsp";
			
			if(request.getParameter("category") != null) {	
				String cat = request.getParameter("category") == null ? "" : request.getParameter("category");
				productList = pu.getProductListByCategory(cat, productMap);
			}
			else if(request.getParameter("keyword") != null) {
				String kw = request.getParameter("keyword") == null ? "" : request.getParameter("keyword");		
				productList = pu.getProductListByKeyword(kw, productMap);
			}
			else {
				productList = pu.getProductList(productMap);
			}
			request.setAttribute("productList",productList);
			System.out.println("productList: "+ productList);  /*debugging*/
		} else {
			request.setAttribute("msg", "請先至後台頁面登入！");
			jspPageToForward = "login";
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
