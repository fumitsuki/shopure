package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.Cart;
import model.CookieUtil;
import model.User;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/UpdateProfile")
public class UpdateProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au =  new AccountUtil();
		CookieUtil cu =  new CookieUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = au.LoginCheck(cookieMap, userMap);
		request.setAttribute("login", login);
		String jspPageToForward = null;
		String page = request.getParameter("submit") == null ? "" : request.getParameter("submit");
		
		if(login) {	
			User user = au.getLoginUser(cookieMap, userMap);
			if ("更改".equals(page)) {	
				String name = request.getParameter("name");
				String address = request.getParameter("address");
				String email = request.getParameter("email");
				String phoneNumber = request.getParameter("phoneNumber");
					
				user = au.updateUser(user.getAccountName(), name, address, email, phoneNumber, userMap);
				getServletContext().setAttribute("userMap", userMap);
				request.setAttribute("msg", "資料修改成功！");
			}
			request.setAttribute("user", user);
			jspPageToForward = "updateProfilePage.jsp";
		}
		else {
			jspPageToForward = "UserLogin";
			request.setAttribute("msg", "請先登入後再更改個人資訊！");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
