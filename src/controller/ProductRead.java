package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AccountUtil;
import model.CookieUtil;
import model.ProductUtil;
import model.User;
import model.Product;

/**
 * 這支Servlet程式扮演Controller的角色
 */
@WebServlet("/product")
public class ProductRead extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		AccountUtil au =  new AccountUtil();
		ProductUtil pu =  new ProductUtil();
		CookieUtil cu =  new CookieUtil();
		HashMap<String, User> userMap = au.mapInit((HashMap<String, User>) getServletContext().getAttribute("userMap"));
		HashMap<String, Product> productMap = pu.mapInit((HashMap<String, Product>) getServletContext().getAttribute("productMap"));
		String id = request.getParameter("productId");
		Map<String, String> cookieMap = cu.getCookieMap(request.getCookies());
		Boolean login = au.LoginCheck(cookieMap, userMap);
		String jspPageToForward = "productPage.jsp";
		
		
		if(id == null || "".equals(id)) {	
			jspPageToForward = "products";
		}
		else if(pu.checkProductExistence(id, productMap) == false) {
			request.setAttribute("msg", "查無此產品！");
			jspPageToForward = "products";
		}
		else {
			Product product = pu.getProduct(id, productMap);
			request.setAttribute("product",product);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(jspPageToForward);
		dispatcher.forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
