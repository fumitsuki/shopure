package controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

public class siteInfoFilter extends HttpFilter {
    @Override
    protected void doFilter(
         HttpServletRequest request, HttpServletResponse response, FilterChain chain)
                throws IOException, ServletException {
    	/*帶入店家資訊與匯款資料*/
    	String[] paramList = {"about", "process","bankName", "bankAccount"};
    	String[] defaultParamValues = {"本賣場貨品優良，貼心為您服務，請放心購物！", 
    			"將商品放入購物車後，請確認下訂，並於指定日內將總金額匯至下方帳戶。賣家確定收款後會將貨物寄出，屆時請耐心等待。", 
    			"中國信託商業銀行" , "576540186536"};
    	int index = 0;
    	for(String param:paramList) {
    		String p = getInitParameter(param); 
        	p = p != null ? p : defaultParamValues[index] ;
        	request.setAttribute(param, p);
        	index++;
    	}
    	
        chain.doFilter(request, response);
    	
    }
}