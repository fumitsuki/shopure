package model;

import java.io.Serializable;

public class Order implements Serializable{

	private String port;
	private java.util.Date date;
	private String[] productId;
	private Integer[] productQty;
	private String accountName;
	private String status;
	
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public java.util.Date getDate() {
		return date;
	}
	public void setDate(java.util.Date date) {
		this.date = date;
	}
	public String[] getProductId() {
		return productId;
	}
	public void setProductId(String[] productId) {
		this.productId = productId;
	}
	public Integer[] getProductQty() {
		return productQty;
	}
	public void setProductQty(Integer[] productQty) {
		this.productQty = productQty;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
