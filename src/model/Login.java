package model;

import java.util.HashMap;
import java.util.Map;

import model.User;

public class Login {
	
	public boolean loginCheck(String id, Map<String, String> loginMap) {
		if(loginMap != null || id != null ) {
			if(loginMap.containsKey("id"))
				return true;
		} 
		return false;
	}

	public User getLogin(String id, Map<String, String> loginMap, HashMap<String, User> hashMap) {
		if(loginMap != null || id != null || hashMap != null ) {
			if(loginMap.containsKey(id)) {
				String accountName = loginMap.get(id);
				if(hashMap.containsKey(accountName))
					return hashMap.get(accountName);
			}
		} 
		return null;
	}
	
	public HashMap<String, User> mapInit(HashMap<String, User> hashMap){
		if (hashMap == null) {
			return new HashMap<String, User>();
		}
		else { 
			return hashMap;
		}
	}

}
