package model;

import java.util.Arrays;
import java.util.HashMap;

import model.Order;
import model.Product;
import model.User;
import model.Cart;

public class CartUtil {
	public boolean checkCartExistence(String accountName,
			HashMap<String, Cart> hashMap) {
		if (hashMap == null)
			return false;
		if (hashMap.containsKey(accountName))
			return true;
		else
			return false;
	}
	
	public Cart cartInit(String accountName,
			   HashMap<String, Cart> hashMap) {
		if(checkCartExistence(accountName, hashMap)) {
			return hashMap.get(accountName);
		}
		Cart cart = new Cart();
		String[] emptyId = {};
		Integer[] emptyQty = {};
		cart.setAccountName(accountName);
		cart.setProductId(emptyId);
		cart.setProductQty(emptyQty);
		hashMap.put(accountName, cart);
		return cart;
	}
	
	public Cart addNewProduct(String accountName,
						   String productId, 
						   Integer productQty, 
						   HashMap<String, Cart> hashMap) {

		Cart cart = cartInit(accountName, hashMap);
		String[] emptyId = {};
		Integer[] emptyQty = {};
		String[] ids = cart.getProductId() == null ? emptyId : cart.getProductId();
		Integer[] qtys = cart.getProductQty() == null ? emptyQty : cart.getProductQty();
		Boolean find = false;
		for(int i = 0;i<ids.length;i++) {
			if(ids[i].equals(productId)) {
				qtys[i] += productQty;
				find = true;
			}
		}
		if(find == false) {
			ids = append(ids, productId);
			qtys = append(qtys, productQty);
		}
		cart.setProductId(ids);
		cart.setProductQty(qtys);
		hashMap.put(accountName, cart);	
		return cart;
	}

	public Cart updateProduct(String accountName,
			   String productId, 
			   Integer productQty, 
			   HashMap<String, Cart> hashMap) {
		
		Cart cart = cartInit(accountName, hashMap);
		String[] emptyId = {};
		Integer[] emptyQty = {};
		String[] ids = cart.getProductId() == null ? emptyId : cart.getProductId();
		Integer[] qtys = cart.getProductQty() == null ? emptyQty : cart.getProductQty();
		Boolean find = false;
		for(int i = 0;i<ids.length;i++) {
		if(ids[i].equals(productId)) {
			qtys[i] = productQty;
			find = true;
		}
		}
		if(find == false) {
		ids = append(ids, productId);
		qtys = append(qtys, productQty);
		}
		cart.setProductId(ids);
		cart.setProductQty(qtys);
		hashMap.put(accountName, cart);	
		return cart;
	}
	
	public boolean deleteProduct(String accountName,
			   String productId, 
			   HashMap<String, Cart> hashMap) {
		
		Cart cart = cartInit(accountName, hashMap);
		String[] emptyId = {};
		Integer[] emptyQty = {};
		String[] ids = cart.getProductId() == null ? emptyId : cart.getProductId();
		Integer[] qtys = cart.getProductQty() == null ? emptyQty : cart.getProductQty();
		String[] newId = new String[ids.length-1];
		Integer[] newQty = new Integer[ids.length-1];
		Integer find = -1;
		for(int i = 0;i<ids.length;i++) {
			if(ids[i].equals(productId)) {
				find = i;
			}
			if(find > -1 && i<ids.length-1) {
				newId[i] = ids[i+1];
				newQty[i] = qtys[i+1];
			}
			else if(i<ids.length-1) {
				newId[i] = ids[i];
				newQty[i] = qtys[i];
			}
		}
		if(find > -1) {
			cart.setProductId(newId);
			cart.setProductQty(newQty);
			hashMap.put(accountName, cart);	
			return true;
		}
		else
			return false;
	}
	
	static <T> T[] append(T[] arr, T element) {
	    final int N = arr.length;
	    arr = Arrays.copyOf(arr, N + 1);
	    arr[N] = element;
	    return arr;
	}
	
	public Cart cleanCart(String accountName,
			   HashMap<String, Cart> hashMap) {
		Cart cart = new Cart();
		String[] emptyId = {};
		Integer[] emptyQty = {};
		cart.setAccountName(accountName);
		cart.setProductId(emptyId);
		cart.setProductQty(emptyQty);
		hashMap.put(accountName, cart);
		return cart;
	}

	
	public HashMap<String, Cart> mapInit(HashMap<String, Cart> hashMap){
		if (hashMap == null) {
			return new HashMap<String, Cart>();
		}
		else { 
			return hashMap;
		}
	}

}
