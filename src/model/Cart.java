package model;

import java.io.Serializable;

public class Cart implements Serializable{

	private String accountName;
	private String[] productId;
	private Integer[] productQty;
	
	public String[] getProductId() {
		return productId;
	}
	public void setProductId(String[] productId) {
		this.productId = productId;
	}
	public Integer[] getProductQty() {
		return productQty;
	}
	public void setProductQty(Integer[] productQty) {
		this.productQty = productQty;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
}
