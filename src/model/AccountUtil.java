package model;

import java.util.HashMap;
import java.util.Map;

import model.User;

public class AccountUtil {
	public User addNewUser(String accountName, 
						   String password, 
						   String name, 
						   String address, 
						   String email, 
						   String phoneNumber,
						   HashMap<String, User> hashMap) {

		User user = new User();
		user.setName(name);
		user.setAddress(address);
		user.setEmail(email);
		user.setPhoneNumber(phoneNumber);
		user.setAccountName(accountName);
		user.setPassword(password);
		
		hashMap.put(accountName, user);
		
		return user;
	}
	
	public User updateUser(String accountName, 
			   String name, 
			   String address, 
			   String email, 
			   String phoneNumber,
			   HashMap<String, User> hashMap) {

		if (hashMap == null)
			return null;
		if(checkAccountNameExistence(accountName, hashMap) == false)
			return null;
		User user = hashMap.get(accountName);
		user.setName(name);
		user.setAddress(address);
		user.setEmail(email);
		user.setPhoneNumber(phoneNumber);
		hashMap.put(accountName, user);
		return user;
	}
	
	public boolean checkAccountNameExistence(String inputAccountName,
			HashMap<String, User> hashMap) {
		if (hashMap == null)
			return false;
		if (hashMap.containsKey(inputAccountName))
			return true;
		else
			return false;
	}
	
	public boolean checkPassword(String inputAccountName, String inputPassword,
			HashMap<String, User> hashMap) {
		if (hashMap == null)
			return false;
		if(checkAccountNameExistence(inputAccountName, hashMap) == false)
			return false;
		User user = hashMap.get(inputAccountName);
		String passwordOnRecord = user.getPassword() == null ? "" : user.getPassword();
		inputPassword = inputPassword == null ? "" : inputPassword;
		if (inputPassword.equals(passwordOnRecord))
			return true;
		else
			return false;

	}
	
	public User getUser(String inputAccountName,  //make sure account is exist & password is correct
			HashMap<String, User> hashMap) {
		return hashMap.get(inputAccountName);
	}
	
	public HashMap<String, User> mapInit(HashMap<String, User> hashMap){
		if (hashMap == null) {
			return new HashMap<String, User>();
		}
		else { 
			return hashMap;
		}
	}
	
	public boolean LoginCheck(Map<String, String> cookie_map, HashMap<String, User> hashMap){
		if(cookie_map != null) {
			if(cookie_map.containsKey("accountName")) {
				String accountName = cookie_map.get("accountName");
				String password = cookie_map.get("password");
				if(checkAccountNameExistence(accountName, hashMap)) 
					if(checkPassword(accountName, password, hashMap))
						return true;
			}
		}	
		return false;
		
	}
	
	public User getLoginUser(Map<String, String> cookie_map, HashMap<String, User> hashMap){
		if(cookie_map != null) {
			if(cookie_map.containsKey("accountName")) {
				String accountName = cookie_map.get("accountName");
				String password = cookie_map.get("password");
				if(checkAccountNameExistence(accountName, hashMap)) 
					if(checkPassword(accountName, password, hashMap))
						return hashMap.get(accountName);
			}
		}	
		return null;
		
	}

}
