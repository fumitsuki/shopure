package model;

import java.util.Map;

public class AdminAccountUtil {
	
	public boolean checkAccount(String inputAccountName,  String inputPassword) {
		if ( inputAccountName == null ||  inputPassword == null)
			return false;
		else if (inputAccountName.equals("admin") && inputPassword.equals("123456"))
			return true;
		else
			return false;
	}
	
	public boolean LoginCheck(Map<String, String> cookie_map){
		if(cookie_map != null) {
			if(cookie_map.containsKey("adminAccountName")) {
				String adminAccountName = cookie_map.get("adminAccountName");
				String adminPassword = cookie_map.get("adminPassword");
				if(checkAccount(adminAccountName, adminPassword)) 
					return true;
			}
		}	
		return false;
		
	}

}
