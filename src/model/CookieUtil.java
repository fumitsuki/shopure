package model;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;


public class CookieUtil {
	public Map<String, String> getCookieMap(Cookie[] cookies){
		Map<String, String> cookie_map = new HashMap<String, String>();
		if(cookies != null) {
			for(Cookie c: cookies) {
				cookie_map.put(c.getName(), c.getValue());
			}
		}	
		return cookie_map;
	}

}
