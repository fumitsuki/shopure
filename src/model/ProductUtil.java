package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import model.Product;
import java.util.UUID;

public class ProductUtil {
	public Product addNewProduct(String name,
						String description,
						String filename,
						String costPrice,
						String salePrice,
						String offerPrice,
						String category, 
						HashMap<String, Product> hashMap) {

		Product product = new Product();
		product.setName(name);
		product.setFilename(filename);
		product.setDescription(description);
		product.setCostPrice(costPrice);
		product.setSalePrice(salePrice);
		product.setOfferPrice(offerPrice);
		product.setStatus("未上架");
		if(category == null || "".equals(category)) 
			product.setCategory("無分類");
		else
			product.setCategory(category);
		String id = UUID.randomUUID().toString();
		product.setId(id);
		hashMap.put(id, product);
		
		return product;
	}
	
	public boolean updateProduct(String id,String name,
			String description,
			String filename,
			String costPrice,
			String salePrice,
			String offerPrice,
			String category, 
			HashMap<String, Product> hashMap) {
				
		if(checkProductExistence(id,hashMap)) {
			Product product = getProduct(id,hashMap);
			product.setName(name);
			product.setFilename(filename);
			product.setDescription(description);
			product.setCostPrice(costPrice);
			product.setSalePrice(salePrice);
			product.setOfferPrice(offerPrice);
			if(category == null || "".equals(category)) 
				product.setCategory("無分類");
			else
				product.setCategory(category);
			hashMap.put(id, product);
			return true;
		}
		else {
				return false;
		}
	}
	
	public boolean updateProductStatus(String id, HashMap<String, Product> hashMap) {
		if(checkProductExistence(id,hashMap)) {
			Product product = getProduct(id,hashMap);
			if("已上架".equals(product.getStatus())) 
				product.setStatus("未上架");
			else
				product.setStatus("已上架");
			hashMap.put(id, product);
			return true;
		}
		else {
				return false;
		}
	}
	
	public boolean checkProductExistence(String id,
			HashMap<String, Product> hashMap) {
		if (hashMap == null)
			return false;
		if (hashMap.containsKey(id))
			return true;
		else
			return false;
	}
	
	public Product updateProduct(String id,String name,
									String description,
									String costPrice,
									String salePrice,
									String offerPrice,
									String category, 
									HashMap<String, Product> hashMap) {
		if (hashMap == null)
			return null;
		if(checkProductExistence(id, hashMap) == false)
			return null;
		Product product = hashMap.get(id);
		product.setName(name);
		product.setDescription(description);
		product.setCostPrice(costPrice);
		product.setSalePrice(salePrice);
		product.setOfferPrice(offerPrice);
		product.setCategory(category);
		return product;
	}
	
	public Product getProduct(String id,  //make sure product is exist & password is correct
			HashMap<String, Product> hashMap) {
		if(hashMap == null || id == null)
			return null;
		return hashMap.get(id);
	}
	
	public boolean deleteProduct(String id,  //make sure product is exist & password is correct
			HashMap<String, Product> hashMap) {
		if(hashMap == null || id == null)
			return false;
		else if(checkProductExistence(id, hashMap) == true) {
			hashMap.remove(id);
			return true;
		}
		return false;
	}
	
	public List<Product> getProductList(HashMap<String, Product> hashMap){
		Set<String> keys = hashMap.keySet();
		List<Product> productList = new ArrayList<>();
		for (String key: keys) {
			productList.add(hashMap.get(key));
		}
		return productList;
	}
	
	public List<Product> getProductListByCategory(String category, HashMap<String, Product> hashMap){
		List<Product> productList = getProductList(hashMap);
		List<Product> catProductList = new ArrayList<>();
		for (Product p:productList) {
			if(p.getCategory().equals(category))
				catProductList.add(p);
		}
		return catProductList;
	}
	
	public List<Product> getProductListByKeyword(String keyword, HashMap<String, Product> hashMap){
		List<Product> productList = getProductList(hashMap);
		List<Product> kwProductList = new ArrayList<>();
		for (Product p:productList) {
			if(p.getName().toLowerCase().contains(keyword.toLowerCase()))
				kwProductList.add(p);
		}
		return kwProductList;
	}
	
	
	
	public HashMap<String, Product> mapInit(HashMap<String, Product> hashMap){
		if (hashMap == null) {
			return new HashMap<String, Product>();
		}
		else { 
			return hashMap;
		}
	}
}	
