package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import model.Order;
import model.Product;
import model.User;

public class OrderUtil {
	public Order addNewOrder(String[] productId, 
						   Integer[] productQty, 
						   String accountName,
						   HashMap<String, Order> hashMap) {

		Order order = new Order();
		order.setProductId(productId);
		order.setProductQty(productQty);
		order.setAccountName(accountName);
		order.setStatus("賣家未收到款項");
		java.util.Date date = java.util.Calendar.getInstance().getTime();
		order.setDate(date);
		String port =  UUID.randomUUID().toString();
		order.setPort(port);
		hashMap.put(port, order);
		
		return order;
	}
	
	public Order updateOrderStatus(String port, 
			   HashMap<String, Order> hashMap) {
	
		Order order = hashMap.get(port);
		if(order.getStatus().equals("賣家未收到款項"))
			order.setStatus("尚未寄出貨品");
		else if(order.getStatus().equals("尚未寄出貨品"))
			order.setStatus("貨品已寄出");
		hashMap.put(port, order);
		return order;
	}
	
	public boolean checkPortExistence(String port,
			HashMap<String, Order> hashMap) {
		if (hashMap == null)
			return false;
		if (hashMap.containsKey(port))
			return true;
		else
			return false;
	}
	
	
	public User getUser(String port,
						HashMap<String, Order> orderMap,
						HashMap<String, User> userMap) {
		User user = new User();
		if (orderMap.containsKey(port)) {
			Order order = orderMap.get(port);
			user = userMap.get(order.getAccountName());
			return user;
		}
		return null;
	}
	
	public List<Order> getOrderList(HashMap<String, Order> hashMap){
		Set<String> keys = hashMap.keySet();
		List<Order> orderList = new ArrayList<>();
		for (String key: keys) {
			orderList.add(hashMap.get(key));
		}
		return orderList;
	}
	
	public List<Order> getOrderListByUser(String accountName, HashMap<String, Order> hashMap){
		List<Order> orderList = getOrderList(hashMap);
		List<Order> userOrderList = new ArrayList<>();
		for (Order p:orderList) {
			if(p.getAccountName().equals(accountName))
				userOrderList.add(p);
		}
		return userOrderList;
	}

	
	public List<Product> getProductList(String port,
			HashMap<String, Order> orderMap,
			HashMap<String, Product> productMap) {
		if (orderMap.containsKey(port)) {
			Order order = orderMap.get(port);
			String[] ProductId = order.getProductId();
			List<Product> productList = new ArrayList<>();
			for (String key: ProductId) {
				productList.add(productMap.get(key));
			}
			return productList;
		}
		return null;
	}
	
	public HashMap<String, Order> mapInit(HashMap<String, Order> hashMap){
		if (hashMap == null) {
			return new HashMap<String, Order>();
		}
		else { 
			return hashMap;
		}
	}

}
